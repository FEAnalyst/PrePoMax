﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CaeGlobals;
using FastColoredTextBoxNS;
using CommandLine;
using System.Diagnostics;

namespace PrePoMax.Commands
{
    [Serializable]
    public class CommandsCollection
    {
        
        // Variables                                                                                                                
        private int _currPositionIndex;
        private Controller _controller;
        private List<Command> _commands;
        private string _historyFileNameBin;
        private ViewGeometryModelResults _previousView;
        private List<string> _errors;


        // Properties                                                                                                               
        public int Count { get { return _commands.Count(); } }
        public int CurrPositionIndex { get { return _currPositionIndex; } }
        public List<Command> Commands { get { return _commands; } }
        public bool IsEnableDisableUndoRedoDefined { get { return EnableDisableUndoRedo != null; } }
        public List<string> Errors { get { return _errors; } }


        // Callbacks                                                                                                                
        [NonSerialized] public Action<string> WriteOutput;
        [NonSerialized] public Action ModelChanged_ResetJobStatus;


        // Events                                                                                                                   
        public event Action<string, string> EnableDisableUndoRedo;


        // Constructor                                                                                                              
        public CommandsCollection(Controller controller)
        {
            _controller = controller;
            _currPositionIndex = -1;
            _commands = new List<Command>();
            _historyFileNameBin = Path.Combine(System.Windows.Forms.Application.StartupPath, Globals.HistoryFileName + ".pmh");
            _previousView = ViewGeometryModelResults.Geometry;
            _errors = null;
            //
            WriteToFile();
        }
        public CommandsCollection(Controller controller, CommandsCollection commandsCollection)
            :this(controller)
        {
            _currPositionIndex = commandsCollection._currPositionIndex;
            _commands = commandsCollection._commands;
            _previousView = commandsCollection._previousView;
            //
            WriteToFile();
        }


        // Methods                                                                                                                  
        public bool AddAndExecute(Command command)
        {
            return ExecuteCommand(command, true);
        }
        private void AddCommand(Command command)
        {
            // Remove old commands
            if (_currPositionIndex < _commands.Count - 1)
                _commands.RemoveRange(_currPositionIndex + 1, _commands.Count - _currPositionIndex - 1);
            //
            _commands.Add(command);
        }
        private void CheckModelChanged(Command command)
        {
            if (command is CClear) return;
            //
            if (command is CSaveToPmx) { }
            else if (command is PreprocessCommand)
            {
                _controller.ModelChanged = true;
                ModelChanged_ResetJobStatus?.Invoke();
            }
        }
        public void SetCommands(List<Command> commands)
        {
            _currPositionIndex = -1;
            _commands.Clear();
            //
            foreach (Command command in commands)
            {
                // Add command
                AddCommand(command);
                //
                _currPositionIndex++;
            }
            // Write to file
            WriteToFile();
            //
            OnEnableDisableUndoRedo();
            // Model changed
            _controller.ModelChanged = true;
            ModelChanged_ResetJobStatus?.Invoke();
        }
        private bool ExecuteCommand(Command command, bool addCommand)
        {
            bool result = false;
            // Write to form
            WriteToOutput(command);
            // First execute to check for errors - DO NOT EXECUTE SAVE COMMAND
            if (command is CSaveToPmx || ExecuteCommandWithTimer(command))
            {
                // Add command
                if (addCommand) AddCommand(command);
                // Check model changed
                CheckModelChanged(command);
                // Write to file
                WriteToFile();
                //
                _currPositionIndex++;
                //
                OnEnableDisableUndoRedo();
                //
                result = true;
            }
            
            // Execute the save command at the end to include all changes in the file
            if (command is CSaveToPmx)
            {
                ExecuteCommandWithTimer(command);
                WriteToFile();  // repeat the write in order to save the hash
            }
            //
            return result;
        }
        private bool ExecuteCommandWithTimer(Command command, bool executeWithDialog = false, bool executeSynchronous = false)
        {
            bool result;
            Stopwatch stopwatch = Stopwatch.StartNew();
            //
            if (executeWithDialog && command is ICommandWithDialog cwd)
            {
                result = cwd.ExecuteWithDialog(_controller);
            }
            else
            {
                // Execute asynchronous tasks in synchronous mode
                if (executeSynchronous && command is ICommandAsynchronous ca) result = ca.ExecuteSynchronous(_controller);
                else result = command.Execute(_controller);
            }
            //
            stopwatch.Stop();
            command.TimeSpan = stopwatch.Elapsed;
            //
            return result;
        }
        public void ExecuteAllCommands()
        {
            ExecuteAllCommands(false, false, true, null);
        }
        public void ExecuteAllCommands(bool showImportDialog, bool showMeshDialog, bool regenerateAll)
        {
            ExecuteAllCommands(showImportDialog, showMeshDialog, regenerateAll, null);
        }
        public void ExecuteAllCommandsFromLastSave(bool regenerateAll, CSaveToPmx lastSave)
        {
            ExecuteAllCommands(false, false, regenerateAll, lastSave);
        }
        public void ExecuteAllCommands(bool showImportDialog, bool showMeshDialog, bool regenerateAll, CSaveToPmx lastSave)
        {
            int count = 0;
            bool executeWithDialog;
            _errors = new List<string>();
            //
            foreach (Command command in _commands)
            {
                if (count++ <= _currPositionIndex)
                {
                    // Set working directory while in regeneration mode
                    if (_controller.RegenerationMode)
                    {
                        if (command is CImportFile cImportFile)
                        {
                            string newFileName = Path.Combine(_controller.Settings.GetWorkDirectory(),
                                                              Path.GetFileName(cImportFile.FileName));
                            if (File.Exists(newFileName)) cImportFile.FileName = newFileName;
                        }
                        else if (command is CExportResultHistoryOutput cExportResultHistoryOutput)
                        {
                            string newFileName = Path.Combine(_controller.Settings.GetWorkDirectory(),
                                                              Path.GetFileName(cExportResultHistoryOutput.FileName));
                            cExportResultHistoryOutput.FileName = newFileName;
                        }
                    }
                    // Skip save before writing to form - set lastSave no null
                    if (command is CSaveToPmx)
                    {
                        if (lastSave != null && command == lastSave) lastSave = null;
                        continue;
                    }
                    // Skip post processing before writing to form
                    if (!regenerateAll && !(command is PreprocessCommand))
                        continue;
                    // Try
                    try
                    {
                        // Skip all up to last save
                        if (lastSave != null) { }
                        // Execute
                        else
                        {
                            // Write to form
                            WriteToOutput(command);
                            //
                            executeWithDialog = false;
                            if (command is ICommandWithDialog cwd)
                            {
                                if (showImportDialog && cwd is CImportFile) executeWithDialog = true;
                                else if (showMeshDialog && cwd is CAddMeshSetupItem) executeWithDialog = true;
                                else if (showMeshDialog && cwd is CReplaceMeshSetupItem) executeWithDialog = true;
                            }
                            //
                            ExecuteCommandWithTimer(command, executeWithDialog, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        _errors.Add(command.Name + ": " + ex.Message);
                    }
                    // Check model changed
                    CheckModelChanged(command);
                }
                else break;
            }
            // Report Errors
            if (_errors.Count != 0)
            {
                WriteOutput?.Invoke("");
                WriteOutput?.Invoke("****   Exceptions   ****");                
                foreach (var error in _errors)
                {
                    WriteOutput?.Invoke(error);
                }
                WriteOutput?.Invoke("****   Number of exceptions: " + _errors.Count + "   ****");
            }
            // Write to file
            WriteToFile();
            //
            OnEnableDisableUndoRedo();
        }
        public CSaveToPmx GetLastSaveCommand()
        {
            Command[] reversed = _commands.ToArray().Reverse().ToArray();   // must be like this
            //
            for (int i = 0; i < reversed.Length; i++)
            {
                if (reversed[i] is CSaveToPmx cstp && cstp.IsFileHashUnchanged()) return cstp;
            }
            //
            return null;
        }
        public Command GetLastExecutedCommand(bool regenerateAll)
        {
            Command lastCommand = null;
            foreach (var command in _commands)
            {
                if ((!regenerateAll && !(command is PreprocessCommand)) || command is CSaveToPmx) continue;
                lastCommand = command;
            }
            return lastCommand;
        }
        // Set execution time for async commands
        public void SetLastAnalysisTime(TimeSpan timeSpan)
        {
            Command[] reversed = _commands.ToArray().Reverse().ToArray();   // must be like this
            //
            for (int i = 0; i < reversed.Length; i++)
            {
                if (reversed[i] is CPrepareAndRunJob cprj)
                {
                    cprj.TimeSpan = timeSpan;
                    break;
                }
            }
        }
        public void SetLastOpenResultsTime(TimeSpan timeSpan)
        {
            Command[] reversed = _commands.ToArray().Reverse().ToArray();   // must be like this
            //
            for (int i = 0; i < reversed.Length; i++)
            {
                if (reversed[i] is COpenResults cor)
                {
                    cor.TimeSpan = timeSpan;
                    break;
                }
            }
        }
        // Clear
        public void Clear()
        {
            _currPositionIndex = -1;
            _commands.Clear();
            _previousView = ViewGeometryModelResults.Geometry;
            _errors = null;
            // Write to file
            WriteToFile();
            //
            OnEnableDisableUndoRedo();
            //
            ModelChanged_ResetJobStatus?.Invoke();
        }
        //
        public void SaveToSeparateFiles(string folderName)
        {
            int i = 1;
            string fileName;
            foreach (var command in _commands)
            {
                if (i == 934)
                    i = 934;
                fileName = Path.Combine(folderName, i++.ToString().PadLeft(4, '0') + "_" + command.Name.Replace("/", "") + ".cmd");
                command.DumpToFile(fileName);
            }
        }
        // Undo / Redo
        public void Undo(bool regenerateAll)
        {
            if (IsUndoPossible)
            {
                _currPositionIndex--;
                ExecuteAllCommands(false, false, regenerateAll);   // also rewrites history
                //
                OnEnableDisableUndoRedo();
            }
        }
        public void Redo()
        {
            if (IsRedoPossible)
            {
                //_currPositionIndex++;
                ExecuteCommand(_commands[_currPositionIndex + 1], false);  // also rewrites history
            }
        }
        public void OnEnableDisableUndoRedo()
        {
            string undo = null;
            string redo = null;
            //
            if (IsUndoPossible) undo = _commands[_currPositionIndex].Name;
            if (IsRedoPossible) redo = _commands[_currPositionIndex + 1].Name;
            //
            if (EnableDisableUndoRedo != null) EnableDisableUndoRedo(undo, redo);
        }
        private bool IsUndoPossible
        {
            get { return _currPositionIndex > -1; }
        }
        private bool IsRedoPossible
        {
            get { return _currPositionIndex < _commands.Count - 1; }
        }
        // Write
        private void WriteToOutput(Command command)
        {
            if (command is CClear) return;
            string data = command.GetCommandString();
            if (data.Length > 20) data = data.Substring(20);    // Remove date and time for the write to form
            WriteOutput?.Invoke(data);
        }
        private void WriteToFile()
        {
            if (!_controller.RegenerationMode && _commands.Count > 1)
            {
                // Write to files
                _commands.DumpToFile(_historyFileNameBin);
                // Use other file
                string fileName = Tools.GetNonExistentRandomFileName(System.Windows.Forms.Application.StartupPath, "pmh");
                _commands.DumpToFile(fileName);
                //
                File.Copy(fileName, _historyFileNameBin, true);
                File.Delete(fileName);
            }
        }
        // Read
        public void ReadFromFile(string fileName)
        {
            _commands = Tools.LoadDumpFromFile<List<Command>>(fileName);
            _currPositionIndex = _commands.Count - 1;
        }
        // History files
        public string GetHistoryFileNameBin()
        {
            return _historyFileNameBin;
        }
        public void DeleteHistoryFile()
        {
            if (File.Exists(_historyFileNameBin)) File.Delete(_historyFileNameBin);
        }
    }
}
